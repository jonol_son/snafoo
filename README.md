# snafoo

## How it works:
The purpose of the application is to allow users (employees of the Nerdery) to suggest and vote on upcoming snacks purchased by the OCD team (the requirements are further defined within: Python_ NAT Specification.pdf).  The app is fed some data through an API provided by the nerdery OCD team.  When the API goes down, the voting, suggestion, and shopping list site will show and error and not allow voting or suggestions to ensure the app and the API remain in sync. 

Views:
	The app is composed of three views: Voting, Suggestions, and Shopping list.  Each views role is defined below by view.

### Voting:
On the voting page, a user is able to see the the snacks always purchased on the left, and vote on Snacks which have been suggested on the right.  Each suggested snack can be voted on once, and each user can vote for three snacks each month.  

### Suggestions:
On the suggestions page a user can suggest a snack to add it to the list in the voting page.  The user can either choose a previously suggested snack (found in the top section) or suggest a new snack by filling in the section below.  To suggest a new snack a user must fill in both a purchase location and the name of the snack.  

### Shopping List:
The final page is simply a shopping list composed of all snacks in the database and where they can be purchased.  

## To build locally:
Set Up a virtual environment:
virtualenv -p /usr/local/bin/python3 env
source env/bin/activate

Install requirements:
pip install -r requirements.txt

## Run Locally:
python manage.py runserver
python manage.py shell

## Deploying to Heroku:
I am currently the only admin (and I am not paying for it so I doubt they would allow other users).  The build process is as straightforward as pushing to Heroku Git (https://dashboard.heroku.com/apps/lit-journey-63573/deploy/heroku-git).  Basically:

1) Download Heroku CLI:
https://devcenter.heroku.com/articles/heroku-cli

2) Login to Heroku and add SSH key:

$ heroku login

3) Clone repo:

$ heroku git:clone -a lit-journey-63573
$ cd lit-journey-63573

4) Push and Deploy:

$ git add .
$ git commit -am "make it better"
$ git push heroku master